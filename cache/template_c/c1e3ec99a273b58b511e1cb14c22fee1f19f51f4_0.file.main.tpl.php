<?php
/* Smarty version 3.1.29, created on 2018-03-01 14:21:00
  from "/var/www/clients/client2/web3/web/application/themes/Social/main.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a97f02c497229_15504866',
  'file_dependency' => 
  array (
    'c1e3ec99a273b58b511e1cb14c22fee1f19f51f4' => 
    array (
      0 => '/var/www/clients/client2/web3/web/application/themes/Social/main.tpl',
      1 => 1517765190,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:sys/meta.tpl' => 1,
    'file:sys/js_css_inc.tpl' => 1,
    'file:sys/js_conf_inc.tpl' => 1,
    'file:sys/js_inc.tpl' => 3,
  ),
),false)) {
function content_5a97f02c497229_15504866 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/var/www/clients/client2/web3/web/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<!DOCTYPE html>
<html lang="en">
<head <?php if (isset($_smarty_tpl->tpl_vars['post']->value)) {?>prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#"<?php }?>>
	<title><?php echo $_smarty_tpl->tpl_vars['pageTitle']->value;?>
</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
">

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/meta.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


	
	<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"><?php echo '</script'; ?>
>-->
</head>
<body>
	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_css_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('place'=>"header"), 0, false);
?>

	<style>
    	body{
    		abackground: #fff;
    	}
    	.popup_layer{
			width: calc( 100% - 68px );
		}
    	.page-content{
    		width: calc( 100% - 68px );
    	}
    	.left-wrap .app-bar{
    		background: #222;
    	}
    	.main-col{
    		max-width: 1000px;
    	}
    	@media screen and (max-width: 1024px){
    		.main-col{
    			max-width: 100%;
    		}
    		.main-col .sidebar{
    			width: 100%;
    		}
    	}
    	@media screen and (max-width: 768px){
    		.page-content{
    			width: 100%;
    		}
			.thumbs{
				width: 100%;
			}
			.thumbs .thumb{
				display: block;
			}
			.thumbs .thumb .image-holder{
				width: 100%;
			}
			.thumb .image-holder .time{
				left: 0px;
			}
			.thumb .content-holder .info{
				position: relative;
				bottom: auto;
			}
    	}
		div.send-form-area{
			background: #f4f4f4;
			border: none;
		}

		.send-post-form .send-form-area{
			padding-right: 45px;
		}

		.send-post-form .avatar{
			padding-left: 0px;
		}
    </style>

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_conf_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

	<!--
	<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="/<?php echo $_smarty_tpl->tpl_vars['path']->value;?>
scripts/jquery-3.2.1.min.js"><?php echo '</script'; ?>
>
-->
	<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"><?php echo '</script'; ?>
>
	<?php if (isset($_smarty_tpl->tpl_vars['post']->value)) {?>
	<?php echo '<script'; ?>
 async defer src="//platform.instagram.com/en_US/embeds.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 async src="//platform.twitter.com/widgets.js" charset="utf-8"><?php echo '</script'; ?>
>
	<?php }?>
	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('place'=>"init"), 0, false);
?>

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('place'=>"header"), 0, true);
?>


	<div class="popup_layer">
		
	</div>

	<div class="popup_layer"></div>

	<div class="main">
		<div class="fll left-wrap">
			<!-- begin app-bar -->
			<div class="app-bar full-viewport-height main-flex fll">
				<nav role="navigation" class="left-nav simple-scrollbar">
					<ul class="flex-fill">
						<li>
							<a href="/" class="app-bar-link">
								<span class="md-icon">assignment</span>
								<span class="app-bar-text">Новости</span>
							</a>
						</li>
						<li class="mobile-hide">
							<a href="javascript:void(0)" data-id="notify" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">alarm</span>
								<span class="app-bar-text">Действия</span>
							</a>
						</li>
						<?php if ($_smarty_tpl->tpl_vars['oUser']->value->id) {?>
						<li>
							<a href="/blog/add" class="app-bar-link">
								<span class="md-icon">add</span>
								<span class="app-bar-text">Добавить</span>
							</a>
						</li>
						<?php }?>
						<li class="mobile-show">
							<a href="javascript:void(0)" data-id="other" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">menu</span>
								<span class="app-bar-text">Другое</span>
							</a>
						</li>
						<li class="mobile-hide spacer"></li>
						<?php if ($_smarty_tpl->tpl_vars['oUser']->value->id) {?>
						<li class="mobile-hide">
							<a href="javascript:void(0)" data-id="settings" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">settings</span>
								<span class="app-bar-text">Настройки</span>
							</a>
						</li>
						<?php }?>
						<li class="mobile-hide <?php if (!$_smarty_tpl->tpl_vars['oUser']->value->id) {?>login-button<?php }?>">
							<a href="javascript:void(0)" class="app-bar-link avatar">
								<?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['oUser']->value),$_smarty_tpl);?>

							</a>
						</li>
					</ul>
				</nav>
			</div>
			<!-- end app-bar -->

			<!-- begin app-content -->
			<!--
			<div class="app-content full-viewport-height fll">
				<div class="loader">
					<a><i class="fa fa-circle-o-notch fa-spin fa-3x"></i></a>
				</div>
				<div class="app-content-wrap">
					<div class="search">
						<div class="app-top">
							<div class="search-flex">
								<form action="#" class="search-form">
									<div class="group">
										<input class="field text-field search-field" type="text" placeholder="Поиск">
										<button class="icon icon-search search-but"></button>
									</div>
								</form>
								<button class="icon icon-pencil app-icon search-outer"></button>
							</div>
						</div>
					</div>
					<div class="tab-content"></div>
				</div>
			</div>
			<!-- end app-content -->
		</div>
		<div class="page-content">
			<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>

	<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/js_inc.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('place'=>"footer"), 0, true);
?>


	<?php echo '<script'; ?>
>
		var sidebar = '<?php echo $_smarty_tpl->tpl_vars['sidebar']->value;?>
';
		//load_sidebar(sidebar);
		jQuery(document).on('click', '[data-type="actions"]', function(e){
			e.preventDefault();
			var list = jQuery(this).parent().find('.actions-menu');
			if(jQuery('body').width()<480){
				list.width(jQuery(this).parent().parent().width());
			}
			if(list.hasClass('show')){
				list.removeClass('show');
			}else{
				list.addClass('show');
				if(!list.hasClass('loaded')){
					jQuery.post('ajax/user/actions/'+jQuery(this).attr('data-user'), {}, function(data){
						list.addClass('loaded');
						data = eval('('+data+')');
						list.html(data.html);
					})
				}
				jQuery('.actions-menu').removeClass('show');
				list.toggleClass('show');
			}
		})
		jQuery(document).on('click', '.tree-header', function(e){
			var o = jQuery(this).find('.icon');
			var list = jQuery(this).parent().find(' > ul');
			if(o.hasClass('icon-down-dir')){
				list.hide();
				o.addClass('icon-right-dir').removeClass('icon-down-dir');
			}else{
				list.show();
				o.removeClass('icon-right-dir').addClass('icon-down-dir');
			}
		})
		jQuery(document).on('click', '.sapp-bar li a', function(e){
			e.preventDefault();
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(sidebar !== jQuery(this).attr('data-id'))
					return false;
				jQuery('body').css('overflow', 'auto');
				width = o.find('.app-content-wrap').width();
				o.animate({ left: '-'+parseInt(width + 20) }, 200).removeClass('show');
			}else{
				if(jQuery('body').width() < 480){
					jQuery('body').css('overflow', 'hidden');
					o.animate({ left: '0px' }, 200).addClass('show');
				}else if(jQuery('body').width() > 480 && jQuery('body').width() < 768){
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		})
		window.onresize = function(e) {
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(jQuery('body').width() < 480){
					o.animate({ left: '0px' }, 200).addClass('show');
				}else{
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		}

		$(".login-button").click(function(e){
			e.preventDefault();
			popup.show('login');
		})
	<?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
>
		
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-103707761-1', 'auto');
	  ga('send', 'pageview');
	  
	<?php echo '</script'; ?>
>
</body>
</html><?php }
}
