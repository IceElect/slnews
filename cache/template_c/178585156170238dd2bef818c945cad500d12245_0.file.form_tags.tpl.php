<?php
/* Smarty version 3.1.29, created on 2017-10-03 10:19:01
  from "/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/default/form_tags.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_59d339e54f1623_38818572',
  'file_dependency' => 
  array (
    '178585156170238dd2bef818c945cad500d12245' => 
    array (
      0 => '/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/default/form_tags.tpl',
      1 => 1506496119,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59d339e54f1623_38818572 ($_smarty_tpl) {
if (!is_callable('smarty_function_translate')) require_once '/media/second_hdd1/isp_clients/client10/web28/web/application/third_party/Smarty-3.1.29/libs/plugins/function.translate.php';
?>
<fieldset class="row">
    <label for="tags" id="def_tags">Теги</label>
    <div class="col field_wrap_text">
        <ul class="tags" id="tags">
            <?php if ((!empty($_smarty_tpl->tpl_vars['aData']->value['tag']))) {?>
                <?php
$_from = $_smarty_tpl->tpl_vars['aData']->value['tag'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tag_0_saved_item = isset($_smarty_tpl->tpl_vars['tag']) ? $_smarty_tpl->tpl_vars['tag'] : false;
$__foreach_tag_0_saved_key = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['tag'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['tag']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['tag']->value) {
$_smarty_tpl->tpl_vars['tag']->_loop = true;
$__foreach_tag_0_saved_local_item = $_smarty_tpl->tpl_vars['tag'];
?>
                    <li><?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
</li>
                <?php
$_smarty_tpl->tpl_vars['tag'] = $__foreach_tag_0_saved_local_item;
}
if ($__foreach_tag_0_saved_item) {
$_smarty_tpl->tpl_vars['tag'] = $__foreach_tag_0_saved_item;
}
if ($__foreach_tag_0_saved_key) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_tag_0_saved_key;
}
?>
            <?php }?>
        </ul>
        <p class="form-text text-muted"><?php echo smarty_function_translate(array('code'=>'error_tags_limit','text'=>'Максимальное количество тэгов 10'),$_smarty_tpl);?>
</p>
        <?php echo '<script'; ?>
>
            
                $(function(){
                    $('.tags').tagit({
                        availableTags: <?php echo $_smarty_tpl->tpl_vars['tags']->value;?>
,
                        fieldName: 'tag[]',
                        tagLimit: 10,
                        onTagLimitExceeded: function(){
                            jQuery(this).parent().children('.error').show();
                        },
                        onTagRemoved: function(){
                            jQuery(this).parent().children('.error').hide();
                        }
                    }, 'field');
                })
            
        <?php echo '</script'; ?>
>
    </div>
</fieldset><?php }
}
