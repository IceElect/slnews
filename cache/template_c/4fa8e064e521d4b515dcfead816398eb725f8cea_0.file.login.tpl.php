<?php
/* Smarty version 3.1.29, created on 2018-03-02 20:24:05
  from "/var/www/clients/client2/web3/web/application/themes/Social/popup/login.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a9996c5637003_18526018',
  'file_dependency' => 
  array (
    '4fa8e064e521d4b515dcfead816398eb725f8cea' => 
    array (
      0 => '/var/www/clients/client2/web3/web/application/themes/Social/popup/login.tpl',
      1 => 1506496133,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a9996c5637003_18526018 ($_smarty_tpl) {
?>
<div class="popup_bg"></div>
<div class="popup block" data-id="login" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Войти в аккаунт <button class="md-icon close" onclick="popup.hide('avatar_upload');">close</button></div>
    </div>
    <div class="popup_content module_content">
		<form method="post" action="/user/login" class="form" data-act="login" data-type="ajax">
			<input type="hidden" name="act" value="login">
			<fieldset>
				<label for="login">Логин/E-mail</label>
				<input type="text" name="user_email" id="login" class="field">
			</fieldset>
			<fieldset>
				<label for="pass">Пароль</label>
				<input type="password" name="user_password" id="pass" class="field">
			</fieldset>
			<fieldset>
				<button type="submit" name="submit" value="1" class="button">Войти</button>
				<a class="button gray" onclick="popup.remove('login')">Отмена</a>
				<span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
			</fieldset>
		</form>
        <div class="clearfix"></div>
    </div>
</div><?php }
}
