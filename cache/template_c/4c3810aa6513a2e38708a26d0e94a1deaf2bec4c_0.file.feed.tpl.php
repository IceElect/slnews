<?php
/* Smarty version 3.1.29, created on 2017-10-20 18:23:53
  from "/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/feed.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_59ea15091c8638_03087627',
  'file_dependency' => 
  array (
    '4c3810aa6513a2e38708a26d0e94a1deaf2bec4c' => 
    array (
      0 => '/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/feed.tpl',
      1 => 1508513013,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:blog/post.tpl' => 1,
    'file:blocks/howto.tpl' => 1,
    'file:blocks/teaser.tpl' => 1,
    'file:blocks/counter.tpl' => 1,
  ),
),false)) {
function content_59ea15091c8638_03087627 ($_smarty_tpl) {
?>
<div class="main-col posts">
    <div class="row">
        <h1 itemprop="headline" class="h2"><?php echo $_smarty_tpl->tpl_vars['pageTextTitle']->value;?>
</h1>
    </div>
    <div class="row">
        <ul class="categories">
            <li class="<?php if (!isset($_smarty_tpl->tpl_vars['category_id']->value)) {?>selected<?php }?>"><a href="/">Все</a></li>
            <?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_category_0_saved_item = isset($_smarty_tpl->tpl_vars['category']) ? $_smarty_tpl->tpl_vars['category'] : false;
$__foreach_category_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$__foreach_category_0_saved_local_item = $_smarty_tpl->tpl_vars['category'];
?>
                <li class="<?php if (isset($_smarty_tpl->tpl_vars['category_id']->value)) {
if ($_smarty_tpl->tpl_vars['category']->value->id == $_smarty_tpl->tpl_vars['category_id']->value) {?>selected<?php }
}?>">
                    <a href="/blog/<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->title;?>
</a>
                </li>
            <?php
$_smarty_tpl->tpl_vars['category'] = $__foreach_category_0_saved_local_item;
}
if ($__foreach_category_0_saved_item) {
$_smarty_tpl->tpl_vars['category'] = $__foreach_category_0_saved_item;
}
if ($__foreach_category_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_category_0_saved_key;
}
?>
        </ul>
    </div>
    <div class="thumbs" itemscope itemtype="http://schema.org/Blog">
        <meta itemprop="description" content="Современный блог о новостях в мире игр и IT">
        <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_post_1_saved_item = isset($_smarty_tpl->tpl_vars['post']) ? $_smarty_tpl->tpl_vars['post'] : false;
$__foreach_post_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$__foreach_post_1_saved_local_item = $_smarty_tpl->tpl_vars['post'];
?>
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blog/post.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('post'=>$_smarty_tpl->tpl_vars['post']->value), 0, true);
?>

        <?php
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_1_saved_local_item;
}
if ($__foreach_post_1_saved_item) {
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_1_saved_item;
}
if ($__foreach_post_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_post_1_saved_key;
}
?>
    </div>
    <div class="sidebar">
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/howto.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <div class="thumb padding">
            <h3>Горячие новости</h3>
            <?php
$_from = $_smarty_tpl->tpl_vars['last_news']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_new_2_saved_item = isset($_smarty_tpl->tpl_vars['new']) ? $_smarty_tpl->tpl_vars['new'] : false;
$_smarty_tpl->tpl_vars['new'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['new']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['new']->value) {
$_smarty_tpl->tpl_vars['new']->_loop = true;
$__foreach_new_2_saved_local_item = $_smarty_tpl->tpl_vars['new'];
?>
                <div class="line_new">
                    <a href="/post/<?php echo $_smarty_tpl->tpl_vars['new']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['new']->value->url;?>
" class="image-holder">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['new']->value->image;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['new']->value->title;?>
">
                    </a>
                    <div class="content">
                        <a href="/post/<?php echo $_smarty_tpl->tpl_vars['new']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['new']->value->url;?>
" title="<?php echo $_smarty_tpl->tpl_vars['new']->value->title;?>
"><h3 class="h4"><?php echo $_smarty_tpl->tpl_vars['new']->value->title;?>
</h3></a>
                    </div>
                </div>
            <?php
$_smarty_tpl->tpl_vars['new'] = $__foreach_new_2_saved_local_item;
}
if ($__foreach_new_2_saved_item) {
$_smarty_tpl->tpl_vars['new'] = $__foreach_new_2_saved_item;
}
?>
        </div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/teaser.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/counter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
    <div class="clearfix"></div>
</div><?php }
}
