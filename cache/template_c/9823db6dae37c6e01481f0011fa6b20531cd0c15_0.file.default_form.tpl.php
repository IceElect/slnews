<?php
/* Smarty version 3.1.29, created on 2017-07-12 14:35:50
  from "Z:\home\blog.ru\www\application\themes\Social\default\default_form.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_596633c6d94ed2_24140492',
  'file_dependency' => 
  array (
    '9823db6dae37c6e01481f0011fa6b20531cd0c15' => 
    array (
      0 => 'Z:\\home\\blog.ru\\www\\application\\themes\\Social\\default\\default_form.tpl',
      1 => 1499870135,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:sys/messages.tpl' => 1,
  ),
),false)) {
function content_596633c6d94ed2_24140492 ($_smarty_tpl) {
if (!is_callable('smarty_function_csrf')) require_once 'Z:\\home\\blog.ru\\www\\application\\third_party\\Smarty-3.1.29\\libs\\plugins\\function.csrf.php';
if (!is_callable('smarty_function_translate')) require_once 'Z:\\home\\blog.ru\\www\\application\\third_party\\Smarty-3.1.29\\libs\\plugins\\function.translate.php';
$_smarty_tpl->tpl_vars["required_star"] = new Smarty_Variable("<span class='required'>*</span>", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "required_star", 0);
$_smarty_tpl->tpl_vars["disabled_attr"] = new Smarty_Variable('disabled = "disabled"', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "disabled_attr", 0);
$_smarty_tpl->tpl_vars["readonly_attr"] = new Smarty_Variable('readonly = "readonly"', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "readonly_attr", 0);?><div class="form_wrapper"><?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sys/messages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<form action="<?php echo $_smarty_tpl->tpl_vars['aConf']->value['base_url'];
echo $_smarty_tpl->tpl_vars['aConf']->value['active_module'];?>
/<?php echo $_smarty_tpl->tpl_vars['editAction']->value;?>
/<?php if (!empty($_smarty_tpl->tpl_vars['nId']->value)) {
echo $_smarty_tpl->tpl_vars['nId']->value;
}?>"method="post" id="frm" enctype="multipart/form-data"><?php echo smarty_function_csrf(array(),$_smarty_tpl);
if ($_smarty_tpl->tpl_vars['action']->value != 'add') {?><input id="item_id" type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['nId']->value;?>
"/><?php } else { ?><input type="hidden" name="change_pwd" value="1"/><?php }?><div class="boxedit"> <div class="table"><?php if ($_smarty_tpl->tpl_vars['action']->value != 'add') {?><div class="row"> <div class="label col">ID :</div> <div class="cell30 col infofield"><?php echo $_smarty_tpl->tpl_vars['nId']->value;?>
</div> </div><?php }
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(0, true);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
$_from = $_smarty_tpl->tpl_vars['aFields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_field_0_saved_item = isset($_smarty_tpl->tpl_vars['field']) ? $_smarty_tpl->tpl_vars['field'] : false;
$__foreach_field_0_saved_key = isset($_smarty_tpl->tpl_vars['field_key']) ? $_smarty_tpl->tpl_vars['field_key'] : false;
$_smarty_tpl->tpl_vars['field'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field_key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['field']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['field_key']->value => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
$__foreach_field_0_saved_local_item = $_smarty_tpl->tpl_vars['field'];
if ((!isset($_smarty_tpl->tpl_vars['field']->value['form_disable']))) {
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, true);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);
if (($_smarty_tpl->tpl_vars['field']->value['type'] != 'id') && ($_smarty_tpl->tpl_vars['field']->value['type'] != 'no_edit')) {?><fieldset class="row <?php echo $_smarty_tpl->tpl_vars['field']->value['type'];?>
"> <label for="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="<?php if ($_smarty_tpl->tpl_vars['field']->value['type'] == 'image') {?>button gray<?php }?>"><?php if ((strpos($_smarty_tpl->tpl_vars['field']->value['rules'],'required') !== false)) {
echo $_smarty_tpl->tpl_vars['required_star']->value;
}?> <?php echo $_smarty_tpl->tpl_vars['field']->value['title'];?>
</label> <div class="col field_wrap_<?php echo $_smarty_tpl->tpl_vars['field']->value['type'];?>
"><?php if (($_smarty_tpl->tpl_vars['field']->value['type'] == 'select')) {?><select name="<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
"class="field"id="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['readonly']))) {
echo $_smarty_tpl->tpl_vars['readonly_attr']->value;
}?> <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['disabled']))) {
echo $_smarty_tpl->tpl_vars['disabled_attr']->value;
}?> ><?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['options']))) {
$_from = $_smarty_tpl->tpl_vars['field']->value['options'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_option_1_saved_item = isset($_smarty_tpl->tpl_vars['option']) ? $_smarty_tpl->tpl_vars['option'] : false;
$__foreach_option_1_saved_key = isset($_smarty_tpl->tpl_vars['option_key']) ? $_smarty_tpl->tpl_vars['option_key'] : false;
$_smarty_tpl->tpl_vars['option'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['option_key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['option']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['option_key']->value => $_smarty_tpl->tpl_vars['option']->value) {
$_smarty_tpl->tpl_vars['option']->_loop = true;
$__foreach_option_1_saved_local_item = $_smarty_tpl->tpl_vars['option'];
?><option value="<?php echo $_smarty_tpl->tpl_vars['option_key']->value;?>
"  <?php if ((!empty($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value]) && $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value] == $_smarty_tpl->tpl_vars['option_key']->value)) {?> selected <?php }?> ><?php echo $_smarty_tpl->tpl_vars['option']->value;?>
</option><?php
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_1_saved_local_item;
}
if ($__foreach_option_1_saved_item) {
$_smarty_tpl->tpl_vars['option'] = $__foreach_option_1_saved_item;
}
if ($__foreach_option_1_saved_key) {
$_smarty_tpl->tpl_vars['option_key'] = $__foreach_option_1_saved_key;
}
}?></select><?php }
if (($_smarty_tpl->tpl_vars['field']->value['type'] == 'textarea')) {?><textarea rows="4" class="field <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['readonly']))) {
echo $_smarty_tpl->tpl_vars['readonly_attr']->value;?>
 readonly<?php }?>" <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['disabled']))) {
echo $_smarty_tpl->tpl_vars['disabled_attr']->value;
}?>  name="<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
" id="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" /><?php if (isset($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {
echo $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value];
}?></textarea><?php }
if (($_smarty_tpl->tpl_vars['field']->value['type'] == 'text')) {?><input type="text" <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['readonly']))) {
echo $_smarty_tpl->tpl_vars['readonly_attr']->value;
}?> <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['disabled']))) {
echo $_smarty_tpl->tpl_vars['disabled_attr']->value;
}?>name="<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
" id="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"class="text field <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['readonly']))) {?>readonly<?php }?>"value="<?php if (isset($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {
echo $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value];
} else {
echo $_smarty_tpl->tpl_vars['field']->value['value'];
}?>"/><?php }
if (($_smarty_tpl->tpl_vars['field']->value['type'] == 'checkbox')) {?><input type="checkbox" <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['readonly']))) {
echo $_smarty_tpl->tpl_vars['readonly_attr']->value;
}?> <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['disabled']))) {
echo $_smarty_tpl->tpl_vars['disabled_attr']->value;
}?>name="<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
" id="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"class="checkbox <?php if ((!empty($_smarty_tpl->tpl_vars['field']->value['readonly']))) {?>readonly<?php }?>"value="1" <?php if (!empty($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {?> checked="checked" <?php }?>autocomplete="off"/> <label for="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">&nbsp</label><?php }
if (($_smarty_tpl->tpl_vars['field']->value['type'] == 'image')) {
if ((empty($_smarty_tpl->tpl_vars['field']->value['readonly']) && empty($_smarty_tpl->tpl_vars['field']->value['disabled']))) {?><input type="file" name="<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
" id="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="image"/><?php }?><div class="image_holder"><?php if ((!empty($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value]))) {?><img class="image_preview" src="/<?php echo $_smarty_tpl->tpl_vars['field']->value['image_src'];
echo $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value];?>
"> <button value="" type="button" class="btn_cancel image_remove" onclick="fields_remove_image(this,<?php echo $_smarty_tpl->tpl_vars['nId']->value;?>
,'<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
')"><?php echo smarty_function_translate(array('code'=>"form_delete",'text'=>"Удалить"),$_smarty_tpl);?>
</button><?php }?></div><?php }
if (($_smarty_tpl->tpl_vars['field']->value['type'] == 'wysiwyg')) {?><textarea id="wysiwyg_<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
" id="def_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"name="<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
"><?php if (isset($_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value])) {
echo $_smarty_tpl->tpl_vars['aData']->value[$_smarty_tpl->tpl_vars['field_key']->value];
}?></textarea>
                                                <?php echo '<script'; ?>
 type="text/javascript">
                                                /*
                                                    tinymce.init({
                                                        selector: "#wysiwyg_<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
"
                                                    });*/
                                                    var ckeditor_<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
 = CKEDITOR.replace('wysiwyg_<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
');
                                                    //CKFinder.setupCKEditor( editor );
                                                    //editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
                                                    CKFinder.setupCKEditor(ckeditor_<?php echo $_smarty_tpl->tpl_vars['field_key']->value;?>
, '/ckfinder/');
                                                <?php echo '</script'; ?>
>
                                            <?php }?></div> </fieldset><?php }
}
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_0_saved_local_item;
}
if ($__foreach_field_0_saved_item) {
$_smarty_tpl->tpl_vars['field'] = $__foreach_field_0_saved_item;
}
if ($__foreach_field_0_saved_key) {
$_smarty_tpl->tpl_vars['field_key'] = $__foreach_field_0_saved_key;
}
?></div> </div> <fieldset class="row box-btn form_actions"> <button type="submit" name="submit" value="1" class="button"><?php echo smarty_function_translate(array('code'=>"form_save",'text'=>"Сохранить"),$_smarty_tpl);?>
</button> <a class="button gray" href="<?php echo $_smarty_tpl->tpl_vars['aConf']->value['base_url'];
echo $_smarty_tpl->tpl_vars['aConf']->value['active_module'];?>
"><?php echo smarty_function_translate(array('code'=>"form_cancel",'text'=>"Отмена"),$_smarty_tpl);?>
</a> </fieldset> </form> </div><?php echo '<script'; ?>
 type="text/javascript">
        jQuery().ready(function () {
            $('#submit').click(function () {
                $('#frm').submit();
            });
        }); // end document.ready

        function fields_remove_image(el, id, field_name) {
            $.ajax({
                type: 'POST',
                url: '<?php echo $_smarty_tpl->tpl_vars['aConf']->value['base_url'];
echo $_smarty_tpl->tpl_vars['aConf']->value['active_module'];?>
/remove_file/' + id + '/' + field_name,
                dataType: 'json',
                success: function (data) {
                    $(el).parent('.image_holder').hide();
                },
                data: {'ajax': 1},
                async: false
            });
        }
        <?php echo '</script'; ?>
><?php }
}
