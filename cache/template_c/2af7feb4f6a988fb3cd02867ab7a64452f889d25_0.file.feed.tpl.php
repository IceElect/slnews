<?php
/* Smarty version 3.1.29, created on 2017-07-12 15:44:04
  from "Z:\home\blog.ru\www\application\themes\Social\feed.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_596643c4a42ca6_26168348',
  'file_dependency' => 
  array (
    '2af7feb4f6a988fb3cd02867ab7a64452f889d25' => 
    array (
      0 => 'Z:\\home\\blog.ru\\www\\application\\themes\\Social\\feed.tpl',
      1 => 1499874242,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_596643c4a42ca6_26168348 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'Z:\\home\\blog.ru\\www\\application\\third_party\\Smarty-3.1.29\\libs\\plugins\\modifier.date_format.php';
?>
<div class="main-col posts">
    <div class="row">
        <ul class="categories">
            <li class="<?php if (!isset($_smarty_tpl->tpl_vars['category_id']->value)) {?>selected<?php }?>"><a href="/">Все</a></li>
            <?php
$_from = $_smarty_tpl->tpl_vars['categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_category_0_saved_item = isset($_smarty_tpl->tpl_vars['category']) ? $_smarty_tpl->tpl_vars['category'] : false;
$__foreach_category_0_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['category'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['category']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
$__foreach_category_0_saved_local_item = $_smarty_tpl->tpl_vars['category'];
?>
                <li class="<?php if (isset($_smarty_tpl->tpl_vars['category_id']->value)) {
if ($_smarty_tpl->tpl_vars['category']->value->id == $_smarty_tpl->tpl_vars['category_id']->value) {?>selected<?php }
}?>"><a href="/blog/<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->title;?>
</a></li>
            <?php
$_smarty_tpl->tpl_vars['category'] = $__foreach_category_0_saved_local_item;
}
if ($__foreach_category_0_saved_item) {
$_smarty_tpl->tpl_vars['category'] = $__foreach_category_0_saved_item;
}
if ($__foreach_category_0_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_category_0_saved_key;
}
?>
        </ul>
    </div>
    <div class="sidebar">
        <div class="thumb padding">
            <h3>Как стать автором?</h3>
            <aside>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nihil, minima magnam voluptas repellat, sed expedita enim in officia alias error? Non voluptatum asperiores, est sed iure dolores nam adipisci!
            </aside>
        </div>
    </div>
    <div class="thumbs">
        <?php
$_from = $_smarty_tpl->tpl_vars['posts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_post_1_saved_item = isset($_smarty_tpl->tpl_vars['post']) ? $_smarty_tpl->tpl_vars['post'] : false;
$__foreach_post_1_saved_key = isset($_smarty_tpl->tpl_vars['i']) ? $_smarty_tpl->tpl_vars['i'] : false;
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$__foreach_post_1_saved_local_item = $_smarty_tpl->tpl_vars['post'];
?>
        <div class="thumb">
            <a href="/post/<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
" class="image-holder">
                <abbr title='<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%Y-%m-%d %H:%M:%S");?>
' class="time"></abbr>
                <img src="<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
" alt="">
                <div class="clearfix"></div>
            </a>
            <div class="content-holder">
                <a href="/post/<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
" class="title"><h3><?php echo $_smarty_tpl->tpl_vars['post']->value->title;?>
</h3></a>
                <p class="description"><?php echo $_smarty_tpl->tpl_vars['post']->value->description;?>
</p>

                <div class="info">
                    <span><i class="fa fa-eye"></i> <?php echo $_smarty_tpl->tpl_vars['post']->value->views_count;?>
</span>
                    <span class="dot"></span>
                    <span><i class="fa fa-comment"></i> <?php echo $_smarty_tpl->tpl_vars['post']->value->comments_count;?>
</span>
                </div>
            </div>
        </div>
        <?php
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_1_saved_local_item;
}
if ($__foreach_post_1_saved_item) {
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_1_saved_item;
}
if ($__foreach_post_1_saved_key) {
$_smarty_tpl->tpl_vars['i'] = $__foreach_post_1_saved_key;
}
?>
    </div>
    <div class="clearfix"></div>
</div><?php }
}
