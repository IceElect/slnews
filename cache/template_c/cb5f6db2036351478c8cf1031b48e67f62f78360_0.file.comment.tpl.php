<?php
/* Smarty version 3.1.29, created on 2018-03-01 18:08:22
  from "/var/www/clients/client2/web3/web/application/themes/Social/blog/comment.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a9825769a15a9_22170163',
  'file_dependency' => 
  array (
    'cb5f6db2036351478c8cf1031b48e67f62f78360' => 
    array (
      0 => '/var/www/clients/client2/web3/web/application/themes/Social/blog/comment.tpl',
      1 => 1517765484,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a9825769a15a9_22170163 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/var/www/clients/client2/web3/web/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
if (!is_callable('smarty_modifier_date_format')) require_once '/var/www/clients/client2/web3/web/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
?>
<div class="comment">
    <div class="avatar middle">
        <?php echo smarty_function_get_avatar(array('u_id'=>$_smarty_tpl->tpl_vars['comment']->value->author_id,'u_av'=>$_smarty_tpl->tpl_vars['comment']->value->avatar),$_smarty_tpl);?>

    </div>
    <div class="content">
        <div class="info">
            <a href="#" class="name"><?php echo $_smarty_tpl->tpl_vars['comment']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['comment']->value->lname;?>
</a>
            <span><abbr title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['comment']->value->date,"%Y-%m-%d %H:%I:%S");?>
" class="time"></abbr></span>
            <span class="spacer"></span>
            <div class="actions">
                <button class="icon fa icon-dot-3"></button>
            </div>
        </div>
        <div class="text">
            <?php echo $_smarty_tpl->tpl_vars['comment']->value->text;?>

        </div>
    </div>
</div><?php }
}
