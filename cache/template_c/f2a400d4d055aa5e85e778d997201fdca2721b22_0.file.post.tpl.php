<?php
/* Smarty version 3.1.29, created on 2018-02-04 19:29:54
  from "/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/post.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a7743125e5f18_92688003',
  'file_dependency' => 
  array (
    'f2a400d4d055aa5e85e778d997201fdca2721b22' => 
    array (
      0 => '/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/post.tpl',
      1 => 1517765229,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:blog/comment.tpl' => 1,
    'file:blocks/howto.tpl' => 1,
    'file:blocks/teaser.tpl' => 1,
    'file:blocks/counter.tpl' => 1,
  ),
),false)) {
function content_5a7743125e5f18_92688003 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/media/second_hdd1/isp_clients/client10/web28/web/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
if (!is_callable('smarty_function_get_avatar')) require_once '/media/second_hdd1/isp_clients/client10/web28/web/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
?>
<div class="main-col post-page">
    <div class="thumbs">
        <article class="thumb big" itemscope itemtype="http://schema.org/Article">
            <div class="image-holder" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                <img itemprop="url contentUrl" src="<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['post']->value->title;?>
">
            </div>
            <div class="content-holder">
                <div class="info">
                    <meta itemprop="genre" content='<?php echo $_smarty_tpl->tpl_vars['category']->value['title'];?>
' />
                    <meta itemprop="datePublished" content='<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%Y-%m-%d");?>
' />
                    <span class="time"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%d %B %Y");?>
</span>
                    <a href="http://m.slto.ru/@<?php echo $_smarty_tpl->tpl_vars['author']->value->id;?>
" class="user <?php if (($_smarty_tpl->tpl_vars['author']->value->last_action >= ($_smarty_tpl->tpl_vars['time']->value-900))) {?>online<?php }?>">
                        <span class="avatar middle">
                            <?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['author']->value),$_smarty_tpl);?>

                        </span>
                        <span class="name" itemprop="author"><?php echo $_smarty_tpl->tpl_vars['author']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['author']->value->lname;?>
</span>
                    </a>
                    <span class="dot"></span>
                    <span><i class="fa fa-eye"></i> <?php echo $_smarty_tpl->tpl_vars['post']->value->views_count;?>
</span>
                    <div class="clearfix"></div>
                </div>
                <h1 class="h2" itemprop="headline"><?php echo $_smarty_tpl->tpl_vars['post']->value->title;?>
</h1>
                <meta itemprop="description" content="<?php echo $_smarty_tpl->tpl_vars['post']->value->description;?>
">
                <div class="content" itemprop="articleBody">
                    <?php echo $_smarty_tpl->tpl_vars['post']->value->text;?>

                </div>
                <!--
                <div class="actions">
            <div class="action">
                
                
                <span class="like green circle active"><i class="icon-angle-up"></i></span>
                                    <span class="small ">0</span>
                                <span class="dislike circle "><i class="icon-angle-down"></i></span>
            </div>
            <div class="action share ">
                <i class="circle icon-forward-outline"></i>
                                <span></span>
            </div>
            <div class="spacer"></div>
            <div class="action comment">
                                    <span class="small">1</span>
                                <i class="comment circle icon-comment-2"></i>
            </div>
        </div>
            -->
            <div class="tags-holder">
                <ul class="tags">
                    <?php
$_from = $_smarty_tpl->tpl_vars['tags']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tag_0_saved_item = isset($_smarty_tpl->tpl_vars['tag']) ? $_smarty_tpl->tpl_vars['tag'] : false;
$_smarty_tpl->tpl_vars['tag'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['tag']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
$_smarty_tpl->tpl_vars['tag']->_loop = true;
$__foreach_tag_0_saved_local_item = $_smarty_tpl->tpl_vars['tag'];
?>
                        <li><a href="#"><?php echo $_smarty_tpl->tpl_vars['tag']->value;?>
</a></li>
                    <?php
$_smarty_tpl->tpl_vars['tag'] = $__foreach_tag_0_saved_local_item;
}
if ($__foreach_tag_0_saved_item) {
$_smarty_tpl->tpl_vars['tag'] = $__foreach_tag_0_saved_item;
}
?>
                </ul>
            </div>
            <div class="actions">
                <div class="spacer"></div>
                <div class="action action-social">
                    <?php echo '<script'; ?>
 type="text/javascript">(function() {
                      if (window.pluso)if (typeof window.pluso.start == "function") return;
                      if (window.ifpluso==undefined) { window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                        var h=d[g]('body')[0];
                        h.appendChild(s);
                      }})();<?php echo '</script'; ?>
>
                    <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=01" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>
                </div>
            </div>
            </div>
        </article>

        <div class="thumb comments" data-post="<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
">
            <div class="title_line">
                <h3 class="h2">Комментарии</h3><span class="h2 comments-count"><?php echo count($_smarty_tpl->tpl_vars['comments']->value);?>
</span>
            </div>
            <div class="send-comment  <?php if (!$_smarty_tpl->tpl_vars['oUser']->value->id) {?>login-button<?php }?>">
                <div class="send-post-form send-comment-form" data-post-id="<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
">
                    <div class="avatar middle">
                        <?php echo smarty_function_get_avatar(array('u'=>$_smarty_tpl->tpl_vars['oUser']->value),$_smarty_tpl);?>

                    </div>
                    <div onkeypress="onCtrlEnter(event, this);" class="send-form-area" contenteditable="true" placeholder="Написать комментарий"></div>
                    <div class="buttons">
                        <button class="fl-r send-button icon icon-paper-plane" onclick="blog.addComment('<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
');"></button>
                    </div>
                </div>
            </div>
            <div class="comments-list">
                <?php
$_from = $_smarty_tpl->tpl_vars['comments']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_comment_1_saved_item = isset($_smarty_tpl->tpl_vars['comment']) ? $_smarty_tpl->tpl_vars['comment'] : false;
$__foreach_comment_1_saved_key = isset($_smarty_tpl->tpl_vars['c']) ? $_smarty_tpl->tpl_vars['c'] : false;
$_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['c'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['comment']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value => $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->_loop = true;
$__foreach_comment_1_saved_local_item = $_smarty_tpl->tpl_vars['comment'];
?>
                    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blog/comment.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('comment'=>$_smarty_tpl->tpl_vars['comment']->value), 0, true);
?>

                <?php
$_smarty_tpl->tpl_vars['comment'] = $__foreach_comment_1_saved_local_item;
}
if ($__foreach_comment_1_saved_item) {
$_smarty_tpl->tpl_vars['comment'] = $__foreach_comment_1_saved_item;
}
if ($__foreach_comment_1_saved_key) {
$_smarty_tpl->tpl_vars['c'] = $__foreach_comment_1_saved_key;
}
?>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/howto.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <div class="thumb padding">
            <h3>Смотреть также</h3>
            <?php
$_from = $_smarty_tpl->tpl_vars['last_news']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_new_2_saved_item = isset($_smarty_tpl->tpl_vars['new']) ? $_smarty_tpl->tpl_vars['new'] : false;
$_smarty_tpl->tpl_vars['new'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['new']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['new']->value) {
$_smarty_tpl->tpl_vars['new']->_loop = true;
$__foreach_new_2_saved_local_item = $_smarty_tpl->tpl_vars['new'];
?>
                <div class="line_new">
                    <a href="/post/<?php echo $_smarty_tpl->tpl_vars['new']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['new']->value->url;?>
" class="image-holder">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['new']->value->image;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['new']->value->title;?>
">
                    </a>
                    <div class="content">
                        <a href="/post/<?php echo $_smarty_tpl->tpl_vars['new']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['new']->value->url;?>
" title="<?php echo $_smarty_tpl->tpl_vars['new']->value->title;?>
"><h3 class="h4"><?php echo $_smarty_tpl->tpl_vars['new']->value->title;?>
</h3></a>
                    </div>
                </div>
            <?php
$_smarty_tpl->tpl_vars['new'] = $__foreach_new_2_saved_local_item;
}
if ($__foreach_new_2_saved_item) {
$_smarty_tpl->tpl_vars['new'] = $__foreach_new_2_saved_item;
}
?>
        </div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/teaser.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/counter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
    <div class="clearfix"></div>
</div><?php }
}
