<?php
/* Smarty version 3.1.29, created on 2018-03-02 20:29:01
  from "/var/www/clients/client2/web3/web/application/themes/Social/add.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a9997ed12ce76_97686096',
  'file_dependency' => 
  array (
    '365b3906b48b424f309447ad498553a4f72ec4c0' => 
    array (
      0 => '/var/www/clients/client2/web3/web/application/themes/Social/add.tpl',
      1 => 1508510585,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:default/default_form.tpl' => 1,
    'file:blocks/teaser.tpl' => 1,
    'file:blocks/counter.tpl' => 1,
  ),
),false)) {
function content_5a9997ed12ce76_97686096 ($_smarty_tpl) {
?>
<div class="main-col add">
    <div class="row"><h1 class="h2">Создать статью</h1></div>
    <div class="thumbs">
        <!--
        <form class="form" action="" method="post">
            <fieldset>
                <label for="title">Заголовок (от 10 символов)</label>
                <input type="text" name="title" id="title" class="field" placeholder="Заголовок">
            </fieldset>
            <fieldset class="file">
                <label for="image" class="button gray">Выберите файл</label>
                <input type="file" name="image" id="image">
            </fieldset>
            <fieldset>
                <label for="description">Описание (до 100 символов)</label>
                <textarea name="description" id="description" class="field" rows="5"></textarea>
            </fieldset>
            <fieldset>
                <label for="text">Содержание</label>
                <textarea name="text" id="text" class="field" rows="20"></textarea>
            </fieldset>
            <fieldset>
                <label for="category">Категория</label>
                <select name="category_id" id="category" class="field">
                    <option value="1">Новости</option>
                    <option value="2">Статьи</option>
                </select>
            </fieldset>
            <fieldset>
                <button type="submit" name="submit" value="submit" class="button">Опубликовать</button>
            </fieldset>
        </form>
        -->
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:default/default_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('form'=>$_smarty_tpl->tpl_vars['form']->value), 0, false);
?>

    </div>
    <div class="sidebar">
        <div class="thumb padding"><br></div>
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/teaser.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:blocks/counter.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
    <div class="clearfix"></div>
</div><?php }
}
