<?php
/* Smarty version 3.1.29, created on 2017-09-27 17:49:44
  from "/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/popup/login.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_59cbba88cdd162_74560225',
  'file_dependency' => 
  array (
    '537c10207c5d35e7bbf47a32503e31f0bb96d2f8' => 
    array (
      0 => '/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/popup/login.tpl',
      1 => 1506496133,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59cbba88cdd162_74560225 ($_smarty_tpl) {
?>
<div class="popup_bg"></div>
<div class="popup block" data-id="login" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Войти в аккаунт <button class="md-icon close" onclick="popup.hide('avatar_upload');">close</button></div>
    </div>
    <div class="popup_content module_content">
		<form method="post" action="/user/login" class="form" data-act="login" data-type="ajax">
			<input type="hidden" name="act" value="login">
			<fieldset>
				<label for="login">Логин/E-mail</label>
				<input type="text" name="user_email" id="login" class="field">
			</fieldset>
			<fieldset>
				<label for="pass">Пароль</label>
				<input type="password" name="user_password" id="pass" class="field">
			</fieldset>
			<fieldset>
				<button type="submit" name="submit" value="1" class="button">Войти</button>
				<a class="button gray" onclick="popup.remove('login')">Отмена</a>
				<span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
			</fieldset>
		</form>
        <div class="clearfix"></div>
    </div>
</div><?php }
}
