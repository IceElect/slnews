<?php
/* Smarty version 3.1.29, created on 2018-02-04 19:31:25
  from "/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/blog/comment.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a77436d440a47_28564590',
  'file_dependency' => 
  array (
    'ea30e3d0c9e8d664e8659bdf6c24556abb1ae0d0' => 
    array (
      0 => '/media/second_hdd1/isp_clients/client10/web28/web/application/themes/Social/blog/comment.tpl',
      1 => 1517765484,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a77436d440a47_28564590 ($_smarty_tpl) {
if (!is_callable('smarty_function_get_avatar')) require_once '/media/second_hdd1/isp_clients/client10/web28/web/application/third_party/Smarty-3.1.29/libs/plugins/function.get_avatar.php';
if (!is_callable('smarty_modifier_date_format')) require_once '/media/second_hdd1/isp_clients/client10/web28/web/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
?>
<div class="comment">
    <div class="avatar middle">
        <?php echo smarty_function_get_avatar(array('u_id'=>$_smarty_tpl->tpl_vars['comment']->value->author_id,'u_av'=>$_smarty_tpl->tpl_vars['comment']->value->avatar),$_smarty_tpl);?>

    </div>
    <div class="content">
        <div class="info">
            <a href="#" class="name"><?php echo $_smarty_tpl->tpl_vars['comment']->value->fname;?>
 <?php echo $_smarty_tpl->tpl_vars['comment']->value->lname;?>
</a>
            <span><abbr title="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['comment']->value->date,"%Y-%m-%d %H:%I:%S");?>
" class="time"></abbr></span>
            <span class="spacer"></span>
            <div class="actions">
                <button class="icon fa icon-dot-3"></button>
            </div>
        </div>
        <div class="text">
            <?php echo $_smarty_tpl->tpl_vars['comment']->value->text;?>

        </div>
    </div>
</div><?php }
}
