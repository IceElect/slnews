<?php
/* Smarty version 3.1.29, created on 2018-03-01 16:24:03
  from "/var/www/clients/client2/web3/web/application/themes/Social/blog/post.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5a980d0300b949_52832261',
  'file_dependency' => 
  array (
    '4c9c4f39549ecd8ea62701bfd21ecf747bd73f47' => 
    array (
      0 => '/var/www/clients/client2/web3/web/application/themes/Social/blog/post.tpl',
      1 => 1506496117,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a980d0300b949_52832261 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/var/www/clients/client2/web3/web/application/third_party/Smarty-3.1.29/libs/plugins/modifier.date_format.php';
?>
<article itemprop="blogPosts" itemscope itemtype="http://schema.org/BlogPosting">
    <div class="thumb post">
        <abbr title='<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value->date,"%Y-%m-%d %H:%M:%S");?>
' class="time"></abbr>
        <a href="/post/<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
" class="image-holder">
            <img itemprop="image" src="<?php echo $_smarty_tpl->tpl_vars['post']->value->image;?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->title, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value->description, ENT_QUOTES, 'UTF-8', true);?>
">
            <div class="clearfix"></div>
        </a>
        <div class="content-holder">
            <a href="/post/<?php echo $_smarty_tpl->tpl_vars['post']->value->id;?>
-<?php echo $_smarty_tpl->tpl_vars['post']->value->url;?>
" class="title"><h2 class="h3" itemprop="name"><?php echo $_smarty_tpl->tpl_vars['post']->value->title;?>
</h2></a>
            <p class="description" itemprop="description"><?php echo $_smarty_tpl->tpl_vars['post']->value->description;?>
</p>

            <div class="info">
                <span><i class="fa fa-eye"></i> <?php echo $_smarty_tpl->tpl_vars['post']->value->views_count;?>
</span>
                <span class="dot"></span>
                <span><i class="fa fa-comment"></i> <?php echo $_smarty_tpl->tpl_vars['post']->value->comments_count;?>
</span>
            </div>
        </div>
    </div>
</article><?php }
}
