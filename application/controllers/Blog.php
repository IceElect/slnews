<?php

class Blog extends Default_controller
{

    function __construct()
    {
        parent::__construct();
        $this->setActiveModule('user');
        $this->load->model('Blog_model', 'blog');
        $this->blog->setTable('post');

    }

    function index($page = 1){
        $posts = $this->blog->getAllData();

        $this->last_news = $this->blog->getAllData(1, 5);

        $categories = $this->blog->getCategories();

        $this->frontend->setTextTitle('Все записи');

        $this->my_smarty->assign('last_news', $this->last_news);
        $this->my_smarty->assign('posts', $posts);
        $this->my_smarty->assign('categories', $categories);
        $this->frontend->view('feed');
    }

    function post($id, $url){
        $url = urldecode($url);
        $this->load->model('user_model');

        $post = $this->blog->getDataByWhere(array('p.url' => $url, 'p.id' => $id));
        $category = $this->blog->getCategory($post[0]->category_id);
        $author = $this->user_model->getInfo($post[0]->author_id);

        $keywords = 'SLnews, свежие, сегодня, последние, 2017, дня, лента, читать, '.$category['title'];

        $this->blog->set_prefix('blog_');
        $tags = $this->blog->get_tags('post', $id);

        $this->blog->inc_views($id);

        $comments = $this->blog->getComments($id);

        $this->last_news = $this->blog->getDataByWhere(array('p.category_id' => $post[0]->category_id), 'p.*', 1, 5);
        
        $this->frontend->setTitle($post[0]->title.' - SLNews');
        $this->frontend->setTextTitle($post[0]->title);
        $this->frontend->add_meta('description', 'description', $post[0]->description);
        $this->frontend->add_meta('og_image', 'og:image', base_url($post[0]->image), 'property');
        $this->frontend->add_meta('og_type', 'og:type', 'article', 'property');
        $this->frontend->add_meta('article_published_time', 'article:published_time', date('Y-m-d H:i:s', $post[0]->date), 'property');
        $this->frontend->add_meta('article_author', 'article:author', $this->config->item('hosted_url').'@'.$author->id, 'property');
        $this->frontend->add_meta('article_section', 'article:section', $category['title'], 'property');

        $i = 0;
        foreach($tags as $tag){
            $i++;
            $this->frontend->add_meta('article_tag_'.$i, 'article:tag', $tag, 'property');
            $keywords .= ', '.$tag;
        }

        $this->frontend->add_meta('keywords', 'keywords', $keywords);

        $this->my_smarty->assign('last_news', $this->last_news);
        $this->my_smarty->assign('category', $category);
        $this->my_smarty->assign('comments', $comments);
        $this->my_smarty->assign('author', $author);
        $this->my_smarty->assign('tags', $tags);
        $this->my_smarty->assign('post', $post[0]);
        $this->frontend->view('post');
    }

    function addComment($post){
        $response = array('response' => false, 'error' => '');
        if(!$this->oUser->id)
            $response['error'] = 'Гость не может оставлять комментарии';
        if(!$this->input->post('text'))
            $response['error'] = 'Введите текст';

        if(empty($response['error'])){
            $aData = array(
                'post_id' => $post,
                'author_id' => $this->oUser->id,
                'text' => $_POST['text'],
                'date' => time()
            );

            $this->load->model('default_model', 'comment_model');
            $this->comment_model->setTable('comments');

            if($this->comment_model->save($aData, 'add')){
                $aData = (object) $aData;
                $aData->fname = $this->oUser->fname;
                $aData->lname = $this->oUser->lname;
                $aData->last_action = $this->oUser->last_action;
                $aData->new = true;

                $this->my_smarty->assign('comment', $aData);
                $response['post'] = $this->frontend->fetch('blog/comment');
                $response['response'] = true;
            }
        }

        echo $this->frontend->returnJson($response);
    }

    function category($id){
        $posts = $this->blog->getDataByWhere(array('p.category_id' => $id));

        $this->last_news = $this->blog->getAllData(1, 5);

        $category = $this->blog->getCategory($id);
        $categories = $this->blog->getCategories();

        $this->my_smarty->assign('last_news', $this->last_news);
        $this->frontend->setTitle('SLNews - '.$category['title']);
        $this->frontend->setTextTitle($category['title']);
        $this->my_smarty->assign('posts', $posts);
        $this->my_smarty->assign('categories', $categories);
        $this->my_smarty->assign('category_id', $id);
        $this->frontend->view('feed');
    }

    function add($category = 1){
        if(!$this->oUser->id)
            mygoto('/users/login');
        $aData = $this->input->post();

        $this->load->library('fields');
        $this->fields->add_hook('after_save', array($this, '_after_save'));
        $this->frontend->add_js('scripts/add_post.js');
        $this->setFields();
        $this->fields->setHook('form', 'form_tags');
        $form = $this->fields->getForm();
        $form['success_text'] = 'Далее';

        $this->frontend->setTitle('SLNews - Создаие статьи');
        $this->my_smarty->assign('editAction', 'add');
        $this->my_smarty->assign('action', 'add');
        $this->my_smarty->assign('aFields', $form['fields']);
        $this->my_smarty->assign('aData', $aData);
        $this->my_smarty->assign('form', $form);
        $this->setActiveModule('blog');
        $this->frontend->view('add');
    }

    function edit($id = ''){
        $action = (!empty($id))?'edit':'add';
        $aData = $this->blog->getDataByWhere(array('p.id' => $id));
        $aData = (array) $aData[0];

        if($this->input->post('submit')){
            $aData = $this->input->post();
        }

        $this->load->library('fields');
        $this->fields->add_hook('after_save', array($this, '_after_save'));
        $this->setFields();
        $this->fields->setHook('form', 'form_tags');
        $form = $this->fields->getForm($id);
        $form['success_text'] = 'Далее';

        $this->frontend->setTitle('SLNews - Создаие статьи');
        $this->my_smarty->assign('nId', $id);
        $this->my_smarty->assign('editAction', 'edit');
        $this->my_smarty->assign('action', 'edit');
        $this->my_smarty->assign('aFields', $form['fields']);
        $this->my_smarty->assign('aData', $aData);
        $this->my_smarty->assign('form', $form);
        $this->setActiveModule('blog');
        $this->frontend->view('add');
    }

    function _after_save($params){
        $this->load->helper('url');
        $is_validate = true;

        $aData = $params['aData'];
        $aData['author_id'] = $this->user->oUser->id;
        if(empty($params['id'])){
            $aData['date'] = time();
            $aData['url'] = url_title($aData['title']);
        }

        if (!empty($_FILES['image']['name'])){
            $this->load->library('uploader');

            $path = 'news/' . date('Y') . '/';
            $file_name = empty($params['id']) ? uniqid() : $params['id'];
            $this->uploader->set_upload_config(array(
                    'file_name' => $file_name,
                    'upload_path' => $path,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'create_folder' => true,
            ));
            //$this->uploader->set_field_title($this->translate->t('post_image', 'Изображение'));//variant_image
            $image_data = $this->uploader->run('image');

            if ($image_data['error']) {
                $is_validate = false;
                $this->messages->add_tmessages_list('error', $image_data['data'], 'image');
            } else {
                $aData['image'] = '/' . $path . $image_data['data']['file_name'];
            }
        }elseif((empty($_FILES['image']['name']) || !isset($_FILES['image'])) && empty($params['id'])){
            $this->messages->add_tmessages_list('error', array('Выберите изображение'), 'image');
            $is_validate = false;
        }

        if($is_validate){
            $params['id'] = $this->blog->save($aData, 'edit', $params['id']);
            if($params['id']){
                $this->fields->save_tags($params);
                mygoto('/blog/'.$aData['category_id']);
            }
        }
    }

    function remove_file($id, $field){
        
    }

    function setFields(){
        $this->fields->setTable('post');

        $aCategories = $this->blog->getCategories(false);
        $categories = array();
        foreach($aCategories as $cat){
            $categories[$cat->id] = $cat->title;
        }

        //$this->fields->addField_check();
        $this->fields->addField_id();
        $this->fields->addField_text(array(
            'field' => 'title',
            'title' => $this->translate->t('field_video_title', 'Заголовок (<span class="count">от 10 символов</span>)'),
            'translable' => 1,
        ));
        /*$this->fields->addField_text(array(
            'field' => 'url',
            'title' => $this->translate->t('field_url', 'Url'),
            'rules' => 'trim|required|alpha_dash|max_length[100]|callback_unique[url]',
            'prefix' => base_url() . 'video/',
            'description' => 'Данное видео будет доступно по этому адресу.',
        ));*/
        $this->fields->addField_image(array(
            'field' => 'image',
            'rules' => 'trim|required',
            'multiple' => false,
            'title' => $this->translate->t('field_actor_screenshots', 'Выберите файл'),
            'disable_save' => true,
        ));
        $this->fields->addField_text(array(
            'field' => 'description',
            'title' => $this->translate->t('field_video_description', 'Описание (<span class="count">до 100 символов</span>)'),
            'rules' => 'trim|required',
            'table_show' => false,
        ));
        $this->fields->addField_wysiwyg(array(
            'field' => 'text',
            'title' => $this->translate->t('field_video_description', 'Содержание'),
            'rules' => 'trim|required',
            'table_show' => false,
        ));
        $this->fields->addField_select(array(
            'field' => 'category_id',
            'rules' => 'trim',
            'table_align' => 'center',
            'table_width' => 100,
            'title' => $this->translate->t('field_gender', 'Раздел'),
            'options' => $categories,
            'tab' => 1,
        ));
    }
}