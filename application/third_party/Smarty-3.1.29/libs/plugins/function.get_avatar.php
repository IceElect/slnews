<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {translate} function plugin
 *
 * Type:     function<br>
 * Name:     translate<br>
 * Date:     Aug 10, 2014<br>
 * Purpose:  translate text via tranlate library
 * Examples: {translate text="Войти" code="login_button"}
 * Output:   Войти
 * Params:
 * <pre>
 * - code        - (required) - translate code
 * - text        - (required) - translate text
 * </pre>
 *
 * @author Elect (slto.ru/elect)
 * @version 1.0
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 * @uses CI translate library()
 */
function smarty_function_get_avatar($params, $template)
{

    $u = '';
    $u_id = false;
    $u_av = false;
    $src = false;
    $link = false;
    $size = '200';
    $continue = true;
    foreach($params as $_key => $_val) {
        switch ($_key) {
            case 'u':
            case 'src':
            case 'u_id':
            case 'u_av':
            case 'link':
            case 'size':
                $$_key = $_val;
                break;
        }
    }

    $CI =& get_instance();

    $id = ($u)?$u->id:$u_id;
    $avatar = ($u)?$u->avatar:$u_av;

    $src = $CI->config->item('hosted_url') . 'ajax/user/avatar/' . $id . '/' . $size . 'x' . $size . '/' . md5($avatar) .  '/photo.jpg';

    if(!$link){
        $src = "<img src='".$src."'>";
        echo $src;
    }else{
        echo $src;
    }

    return;

    if($src){
        if(@getimagesize( $CI->config->item('hosted_url') . $src)){
            $src = $CI->config->item('hosted_url') . $src;
            $continue = false;
        }else{
            //$src = base_url('no_avatar.png');
            $src = $CI->config->item('hosted_url') . $src;
        }


        $continue = false; // TEST
    }

    if (empty($id)) {
        $id = 0;
    }

    if($continue){
        if($id !== 0){
            $info = $CI->user_model->getInfo($id, "i.album as avatar_album, i.src AS avatar_file");
        }else{
            $info = (object) array('avatar_album' => 'none', 'avatar_file' => 'none');
        }
        if(@getimagesize( $CI->config->item('hosted_url') . $info->avatar_file)){
            $src = $CI->config->item('hosted_url') . $info->avatar_file;
        }else{
            if(@getimagesize( $CI->config->item('hosted_url') .  'albums/' . $info->avatar_album . "/" . $info->avatar_file ) ){
                $src = $CI->config->item('hosted_url') . "albums/".$info->avatar_album."/".$info->avatar_file;
            }else{
                $src = base_url( "no_avatar.png" );
            }
        }
    }

    if(!$link){
        $src = "<img src='".$src."'>";
        echo $src;
    }else{
        echo $src;
    }


}

?>