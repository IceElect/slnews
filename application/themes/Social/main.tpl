<!DOCTYPE html>
<html lang="en">
<head {if isset($post)}prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#"{/if}>
	<title>{$pageTitle}</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="canonical" href="{$base_url}">

	{include file="sys/meta.tpl"}

	
	<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
</head>
<body>
	{include file="sys/js_css_inc.tpl" place="header"}
	<style>
    	body{
    		abackground: #fff;
    	}
    	.popup_layer{
			width: calc( 100% - 68px );
		}
    	.page-content{
    		width: calc( 100% - 68px );
    	}
    	.left-wrap .app-bar{
    		background: #222;
    	}
    	.main-col{
    		max-width: 1000px;
    	}
    	@media screen and (max-width: 1024px){
    		.main-col{
    			max-width: 100%;
    		}
    		.main-col .sidebar{
    			width: 100%;
    		}
    	}
    	@media screen and (max-width: 768px){
    		.page-content{
    			width: 100%;
    		}
			.thumbs{
				width: 100%;
			}
			.thumbs .thumb{
				display: block;
			}
			.thumbs .thumb .image-holder{
				width: 100%;
			}
			.thumb .image-holder .time{
				left: 0px;
			}
			.thumb .content-holder .info{
				position: relative;
				bottom: auto;
			}
    	}
		div.send-form-area{
			background: #f4f4f4;
			border: none;
		}

		.send-post-form .send-form-area{
			padding-right: 45px;
		}

		.send-post-form .avatar{
			padding-left: 0px;
		}
    </style>

	{include file="sys/js_conf_inc.tpl"}
	<!--
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/{$path}scripts/jquery-3.2.1.min.js"></script>
-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	{if isset($post)}
	<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
	<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
	{/if}
	{include file="sys/js_inc.tpl" place="init"}
	{include file="sys/js_inc.tpl" place="header"}

	<div class="popup_layer">
		
	</div>

	<div class="popup_layer"></div>

	<div class="main">
		<div class="fll left-wrap">
			<!-- begin app-bar -->
			<div class="app-bar full-viewport-height main-flex fll">
				<nav role="navigation" class="left-nav simple-scrollbar">
					<ul class="flex-fill">
						<li>
							<a href="/" class="app-bar-link">
								<span class="md-icon">assignment</span>
								<span class="app-bar-text">Новости</span>
							</a>
						</li>
						<li class="mobile-hide">
							<a href="javascript:void(0)" data-id="notify" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">alarm</span>
								<span class="app-bar-text">Действия</span>
							</a>
						</li>
						{if $oUser->id}
						<li>
							<a href="/blog/add" class="app-bar-link">
								<span class="md-icon">add</span>
								<span class="app-bar-text">Добавить</span>
							</a>
						</li>
						{/if}
						<li class="mobile-show">
							<a href="javascript:void(0)" data-id="other" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">menu</span>
								<span class="app-bar-text">Другое</span>
							</a>
						</li>
						<li class="mobile-hide spacer"></li>
						{if $oUser->id}
						<li class="mobile-hide">
							<a href="javascript:void(0)" data-id="settings" data-type="load_sidebar" class="app-bar-link">
								<span class="md-icon">settings</span>
								<span class="app-bar-text">Настройки</span>
							</a>
						</li>
						{/if}
						<li class="mobile-hide {if !$oUser->id}login-button{/if}">
							<a href="javascript:void(0)" class="app-bar-link avatar">
								{get_avatar u=$oUser}
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<!-- end app-bar -->

			<!-- begin app-content -->
			<!--
			<div class="app-content full-viewport-height fll">
				<div class="loader">
					<a><i class="fa fa-circle-o-notch fa-spin fa-3x"></i></a>
				</div>
				<div class="app-content-wrap">
					<div class="search">
						<div class="app-top">
							<div class="search-flex">
								<form action="#" class="search-form">
									<div class="group">
										<input class="field text-field search-field" type="text" placeholder="Поиск">
										<button class="icon icon-search search-but"></button>
									</div>
								</form>
								<button class="icon icon-pencil app-icon search-outer"></button>
							</div>
						</div>
					</div>
					<div class="tab-content"></div>
				</div>
			</div>
			<!-- end app-content -->
		</div>
		<div class="page-content">
			{$content}
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>

	{include file="sys/js_inc.tpl" place="footer"}

	<script>
		var sidebar = '{$sidebar}';
		//load_sidebar(sidebar);
		jQuery(document).on('click', '[data-type="actions"]', function(e){
			e.preventDefault();
			var list = jQuery(this).parent().find('.actions-menu');
			if(jQuery('body').width()<480){
				list.width(jQuery(this).parent().parent().width());
			}
			if(list.hasClass('show')){
				list.removeClass('show');
			}else{
				list.addClass('show');
				if(!list.hasClass('loaded')){
					jQuery.post('ajax/user/actions/'+jQuery(this).attr('data-user'), {}, function(data){
						list.addClass('loaded');
						data = eval('('+data+')');
						list.html(data.html);
					})
				}
				jQuery('.actions-menu').removeClass('show');
				list.toggleClass('show');
			}
		})
		jQuery(document).on('click', '.tree-header', function(e){
			var o = jQuery(this).find('.icon');
			var list = jQuery(this).parent().find(' > ul');
			if(o.hasClass('icon-down-dir')){
				list.hide();
				o.addClass('icon-right-dir').removeClass('icon-down-dir');
			}else{
				list.show();
				o.removeClass('icon-right-dir').addClass('icon-down-dir');
			}
		})
		jQuery(document).on('click', '.sapp-bar li a', function(e){
			e.preventDefault();
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(sidebar !== jQuery(this).attr('data-id'))
					return false;
				jQuery('body').css('overflow', 'auto');
				width = o.find('.app-content-wrap').width();
				o.animate({ left: '-'+parseInt(width + 20) }, 200).removeClass('show');
			}else{
				if(jQuery('body').width() < 480){
					jQuery('body').css('overflow', 'hidden');
					o.animate({ left: '0px' }, 200).addClass('show');
				}else if(jQuery('body').width() > 480 && jQuery('body').width() < 768){
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		})
		window.onresize = function(e) {
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(jQuery('body').width() < 480){
					o.animate({ left: '0px' }, 200).addClass('show');
				}else{
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		}

		$(".login-button").click(function(e){
			e.preventDefault();
			popup.show('login');
		})
	</script>
	<script>
		{literal}
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-103707761-1', 'auto');
	  ga('send', 'pageview');
	  {/literal}
	</script>
</body>
</html>