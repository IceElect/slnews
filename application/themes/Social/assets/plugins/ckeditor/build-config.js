﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) http://ckeditor.com/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) http://ckeditor.com/builder/c6d21551df74d9554265d426b176a092
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) http://ckeditor.com/builder/download/c6d21551df74d9554265d426b176a092
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'moono-lisa',
	preset: 'full',
	ignore: [
		'.DS_Store',
		'.bender',
		'.editorconfig',
		'.gitattributes',
		'.gitignore',
		'.idea',
		'.jscsrc',
		'.jshintignore',
		'.jshintrc',
		'.mailmap',
		'.travis.yml',
		'bender-err.log',
		'bender-out.log',
		'bender.ci.js',
		'bender.js',
		'dev',
		'gruntfile.js',
		'less',
		'node_modules',
		'package.json',
		'tests'
	],
	plugins : {
		'SimpleImage' : 1,
		'SimpleLink' : 1,
		'autolink' : 1,
		'basicstyles' : 1,
		'blockquote' : 1,
		'ckeditor_fa' : 1,
		'clipboard' : 1,
		'colorbutton' : 1,
		'colordialog' : 1,
		'contextmenu' : 1,
		'custimage' : 1,
		'dialogadvtab' : 1,
		'dropdownmenumanager' : 1,
		'elementspath' : 1,
		'enterkey' : 1,
		'filebrowser' : 1,
		'filetools' : 1,
		'find' : 1,
		'floatingspace' : 1,
		'format' : 1,
		'horizontalrule' : 1,
		'htmlwriter' : 1,
		'image' : 1,
		'imageCustomUploader' : 1,
		'imagebrowser' : 1,
		'imagepaste' : 1,
		'imageresize' : 1,
		'imageresponsive' : 1,
		'imagerotate' : 1,
		'imageuploader' : 1,
		'imgbrowse' : 1,
		'indentlist' : 1,
		'insertpre' : 1,
		'justify' : 1,
		'list' : 1,
		'liststyle' : 1,
		'magicline' : 1,
		'maximize' : 1,
		'page2images' : 1,
		'pagebreak' : 1,
		'pasteFromGoogleDoc' : 1,
		'pastebase64' : 1,
		'pastefromexcel' : 1,
		'pastefromword' : 1,
		'pastetext' : 1,
		'resize' : 1,
		'scayt' : 1,
		'showborders' : 1,
		'simple-image-browser' : 1,
		'simpleImageUpload' : 1,
		'stylescombo' : 1,
		'tab' : 1,
		'table' : 1,
		'tableresize' : 1,
		'tableselection' : 1,
		'tabletools' : 1,
		'tabletoolstoolbar' : 1,
		'templates' : 1,
		'toolbar' : 1,
		'uploadfile' : 1,
		'uploadimage' : 1,
		'widgetcontextmenu' : 1,
		'wysiwygarea' : 1
	},
	languages : {
		'en' : 1,
		'ru' : 1
	}
};