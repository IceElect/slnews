<div class="main-col post-page">
    <div class="thumbs">
        <article class="thumb big" itemscope itemtype="http://schema.org/Article">
            <div class="image-holder" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                <img itemprop="url contentUrl" src="{$post->image}" alt="{$post->title}">
            </div>
            <div class="content-holder">
                <div class="info">
                    <meta itemprop="genre" content='{$category.title}' />
                    <meta itemprop="datePublished" content='{$post->date|date_format:"%Y-%m-%d"}' />
                    <span class="time">{$post->date|date_format:"%d %B %Y"}</span>
                    <a href="http://m.slto.ru/@{$author->id}" class="user {if ($author->last_action >= ($time - 900))}online{/if}">
                        <span class="avatar middle">
                            {get_avatar u=$author}
                        </span>
                        <span class="name" itemprop="author">{$author->fname} {$author->lname}</span>
                    </a>
                    <span class="dot"></span>
                    <span><i class="fa fa-eye"></i> {$post->views_count}</span>
                    <div class="clearfix"></div>
                </div>
                <h1 class="h2" itemprop="headline">{$post->title}</h1>
                <meta itemprop="description" content="{$post->description}">
                <div class="content" itemprop="articleBody">
                    {$post->text}
                </div>
                <!--
                <div class="actions">
            <div class="action">
                
                
                <span class="like green circle active"><i class="icon-angle-up"></i></span>
                                    <span class="small ">0</span>
                                <span class="dislike circle "><i class="icon-angle-down"></i></span>
            </div>
            <div class="action share ">
                <i class="circle icon-forward-outline"></i>
                                <span></span>
            </div>
            <div class="spacer"></div>
            <div class="action comment">
                                    <span class="small">1</span>
                                <i class="comment circle icon-comment-2"></i>
            </div>
        </div>
            -->
            <div class="tags-holder">
                <ul class="tags">
                    {foreach $tags item=$tag}
                        <li><a href="#">{$tag}</a></li>
                    {/foreach}
                </ul>
            </div>
            <div class="actions">
                <div class="spacer"></div>
                <div class="action action-social">
                    <script type="text/javascript">(function() {
                      if (window.pluso)if (typeof window.pluso.start == "function") return;
                      if (window.ifpluso==undefined) { window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                        var h=d[g]('body')[0];
                        h.appendChild(s);
                      }})();</script>
                    <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=01" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>
                </div>
            </div>
            </div>
        </article>

        <div class="thumb comments" data-post="{$post->id}">
            <div class="title_line">
                <h3 class="h2">Комментарии</h3><span class="h2 comments-count">{$comments|@count}</span>
            </div>
            <div class="send-comment  {if !$oUser->id}login-button{/if}">
                <div class="send-post-form send-comment-form" data-post-id="{$post->id}">
                    <div class="avatar middle">
                        {get_avatar u=$oUser}
                    </div>
                    <div onkeypress="onCtrlEnter(event, this);" class="send-form-area" contenteditable="true" placeholder="Написать комментарий"></div>
                    <div class="buttons">
                        <button class="fl-r send-button icon icon-paper-plane" onclick="blog.addComment('{$post->id}');"></button>
                    </div>
                </div>
            </div>
            <div class="comments-list">
                {foreach from=$comments key=$c item=$comment}
                    {include file="blog/comment.tpl" comment=$comment}
                {/foreach}
            </div>
        </div>
    </div>
    <div class="sidebar">
        {include file="blocks/howto.tpl"}
        <div class="thumb padding">
            <h3>Смотреть также</h3>
            {foreach from=$last_news item=$new}
                <div class="line_new">
                    <a href="/post/{$new->id}-{$new->url}" class="image-holder">
                        <img src="{$new->image}" alt="{$new->title}">
                    </a>
                    <div class="content">
                        <a href="/post/{$new->id}-{$new->url}" title="{$new->title}"><h3 class="h4">{$new->title}</h3></a>
                    </div>
                </div>
            {/foreach}
        </div>
        {include file="blocks/teaser.tpl"}
        {include file="blocks/counter.tpl"}
    </div>
    <div class="clearfix"></div>
</div>