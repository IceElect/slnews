<fieldset class="row">
    <label for="tags" id="def_tags">Теги</label>
    <div class="col field_wrap_text">
        <ul class="tags" id="tags">
            {if (!empty($aData.tag))}
                {foreach $aData.tag as $key => $tag}
                    <li>{$tag}</li>
                {/foreach}
            {/if}
        </ul>
        <p class="form-text text-muted">{translate code='error_tags_limit' text='Максимальное количество тэгов 10'}</p>
        <script>
            {literal}
                $(function(){
                    $('.tags').tagit({
                        availableTags: {/literal}{$tags}{literal},
                        fieldName: 'tag[]',
                        tagLimit: 10,
                        onTagLimitExceeded: function(){
                            jQuery(this).parent().children('.error').show();
                        },
                        onTagRemoved: function(){
                            jQuery(this).parent().children('.error').hide();
                        }
                    }, 'field');
                })
            {/literal}
        </script>
    </div>
</fieldset>