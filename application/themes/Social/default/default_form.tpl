{strip}
    {assign var="required_star" value="<span class='required'>*</span>"}
    {assign var="disabled_attr" value='disabled = "disabled"'}
    {assign var="readonly_attr" value='readonly = "readonly"'}
    {nocache}
        <div class="form_wrapper">
            {include file="sys/messages.tpl"}
            <form action="{$aConf.base_url}{$aConf.active_module}/{$editAction}/{if !empty($nId) }{$nId}{/if}"
                  method="post" id="frm" enctype="multipart/form-data">
                {csrf}
                {if $action ne 'add'}
                    <input id="item_id" type="hidden" name="id" value="{$nId}"/>
                {else}
                    <input type="hidden" name="change_pwd" value="1"/>
                {/if}
                <div class="boxedit">
                    <div class="table">
                        {if $action ne 'add'}
                            <div class="row">
                                <div class="label col">
                                    ID :
                                </div>
                                <div class="cell30 col infofield">
                                    {$nId}
                                </div>
                            </div>
                        {/if}
                        {$i = 0}
                        {foreach from=$aFields key="field_key" item="field" }
                            {if (!isset($field.form_disable))}
                                {$i = $i +1}
                                {if !empty($field.custom_field_template)}
                                    {assign var=path value=$field.custom_field_template}
                                    {include file="custom/$path.tpl" field=$field aData=$aData i=$i}
                                {else}
                                    {if ($field.type ne 'id') && ($field.type ne 'no_edit')}
                                        <fieldset class="row {$field.type}">
                                            <label for="def_{$i}" class="{if $field.type == 'image'}button gray{/if}">
                                                {if (strpos($field.rules,'required')!== false)}{$required_star}{/if} {$field.title}
                                            </label>
                                            <div class="col field_wrap_{$field.type}">
                                                {if ($field.type == 'select') }
                                                    <select name="{$field_key}"
                                                            class="field"
                                                            id="def_{$i}" {if (!empty($field.readonly)) }{$readonly_attr}{/if} {if (!empty($field.disabled)) }{$disabled_attr}{/if} >
                                                        {if (!empty($field.options))}
                                                            {foreach from=$field.options key="option_key" item="option" }
                                                                <option value="{$option_key}"  {if (!empty($aData.$field_key) && $aData.$field_key == $option_key) } selected {/if} >{$option}</option>
                                                            {/foreach}
                                                        {/if}
                                                    </select>
                                                {/if}
                                                {if ($field.type == 'textarea') }
                                                    <textarea rows="4" class="field {if (!empty($field.readonly))}{$readonly_attr} readonly{/if}" {if (!empty($field.disabled))}{$disabled_attr}{/if}  name="{$field_key}" id="def_{$i}" />{if isset($aData.$field_key) }{$aData.$field_key}{/if}</textarea>
                                                {/if}
                                                {if ($field.type == 'text') }
                                                    <input type="text" {if (!empty($field.readonly))}{$readonly_attr}{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}
                                                           name="{$field_key}" id="def_{$i}"
                                                           class="text field {if (!empty($field.readonly))}readonly{/if}"
                                                           value="{if isset($aData.$field_key) }{$aData.$field_key}{else}{$field.value}{/if}"/>
                                                {/if}
                                                {if ($field.type == 'checkbox') }
                                                    <input type="checkbox" {if (!empty($field.readonly))}{$readonly_attr}{/if} {if (!empty($field.disabled))}{$disabled_attr}{/if}
                                                           name="{$field_key}" id="def_{$i}"
                                                           class="checkbox {if (!empty($field.readonly))}readonly{/if}"
                                                           value="1" {if !empty($aData.$field_key) } checked="checked" {/if}
                                                           autocomplete="off"/>
                                                    <label for="def_{$i}">&nbsp</label>
                                                {/if}
                                                {if ($field.type == 'image') }
                                                    {if (empty($field.readonly) && empty($field.disabled)) }
                                                        <input type="file" name="{$field_key}" id="def_{$i}" class="image"/>
                                                    {/if}
                                                    <div class="image_holder">
                                                        {if (!empty($aData.$field_key)) }
                                                            <img class="image_preview"
                                                                 src="/{$field.image_src}{$aData.$field_key}">
                                                            <button value="" type="button" class="btn_cancel image_remove"
                                                                    onclick="fields_remove_image(this,{$nId},'{$field_key}')">
                                                                {translate code="form_delete" text="Удалить"}
                                                            </button>
                                                        {/if}
                                                    </div>
                                                {/if}
                                                {if ($field.type == 'wysiwyg') }
                                                    <textarea class="wysiwyg_{$field_key}" id="def_{$i}"
                                                              name="{$field_key}">{if isset($aData.$field_key) }{$aData.$field_key}{/if}</textarea>
                                                {literal}
                                                    <!--
                                                    <script>tinymce.init({ selector:'textarea#def_{/literal}{$i}{literal}' });</script>
                                                    <script type="text/javascript">
                                                    /*
                                                        tinymce.init({
                                                            selector: "#wysiwyg_{/literal}{$field_key}{literal}"
                                                        });*/
                                                        //var ckeditor_{/literal}{$field_key}{literal} = CKEDITOR.replace('wysiwyg_{/literal}{$field_key}{literal}');
                                                        //CKFinder.setupCKEditor( editor );
                                                        //editor.setData( '<p>Just click the <b>Image</b> or <b>Link</b> button, and then <b>&quot;Browse Server&quot;</b>.</p>' );
                                                        //CKFinder.setupCKEditor(ckeditor_{/literal}{$field_key}{literal}, '/ckfinder/');
                                                    </script>
                                                -->
                                                    <script>
                                                        tinymce.init({ 
                                                            selector:'textarea#def_{/literal}{$i}{literal}',
                                                            language: 'ru',
                                                            menubar: "",
                                                            toolbar: "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image media hr | fullscreen | spellchecker | searchreplace",
                                                            /*spellchecker_callback: function(method, text, success, failure) {
                                                              tinymce.util.JSONRequest.sendRPC({
                                                                url: "/tinymce/spellchecker.php",
                                                                method: "spellcheck",
                                                                params: {
                                                                  lang: this.getLanguage(),
                                                                  words: text.match(this.getWordCharPattern())
                                                                },
                                                                success: function(result) {
                                                                  success(result);
                                                                },
                                                                error: function(error, xhr) {
                                                                  failure("Spellcheck error:" + xhr.status);
                                                                }
                                                            });
                                                            },*/
                                                            plugins: "image imagetools media autolink link hr wordcount searchreplace spellchecker",
                                                            spellchecker_languages : "+Russian=ru,Ukrainian=uk,English=en",
                                                            spellchecker_language : "ru",
                                                            spellchecker_rpc_url : "//speller.yandex.net/services/tinyspell",
                                                            spellchecker_word_separator_chars : '\\s!"#$%&()*+,./:;<=>?@[\]^_{|}\xa7\xa9\xab\xae\xb1\xb6\xb7\xb8\xbb\xbc\xbd\xbe\u00bf\xd7\xf7\xa4\u201d\u201c'
                                                        });
                                                    </script>
                                                {/literal}
                                                {/if}
                                            </div>
                                        </fieldset>
                                    {/if}
                                {/if}
                            {/if}
                        {/foreach}

                        {if isset($hooks.form)}
                            {foreach from=$hooks.form key=key item=hook}
                                {include file="default/{$hook}.tpl"}
                            {/foreach}
                        {/if}


                    </div>
                </div>

                <fieldset class="row box-btn form_actions">
                    <button type="submit" name="submit" value="1" class="button">
                        {translate code="form_save" text="Сохранить"}
                    </button>
                    <a class="button gray" href="{$aConf.base_url}{$aConf.active_module}">
                        {translate code="form_cancel" text="Отмена"}
                    </a>

                </fieldset>
            </form>
        </div>
    {/nocache}
    <script type="text/javascript">
        {literal}
        jQuery().ready(function () {
            $('#submit').click(function () {
                $('#frm').submit();
            });
        }); // end document.ready

        function fields_remove_image(el, id, field_name) {
            $.ajax({
                type: 'POST',
                url: '{/literal}{$aConf.base_url}{$aConf.active_module}/remove_file/{literal}' + id + '/' + field_name,
                dataType: 'json',
                success: function (data) {
                    $(el).parent('.image_holder').hide();
                },
                data: {'ajax': 1},
                async: false
            });
        }
        {/literal}
    </script>
{/strip}