<div class="nest">
    <div class="title-alt">
        <h6>Категории</h6>
        <div class="titleClose">
            <a class="gone" href="#elementClose">
                <span class="entypo-cancel"></span>
            </a>
        </div>
        <div class="titleToggle">
            <a class="nav-toggle-alt" href="#element">
                <span class="entypo-up-open"></span>
            </a>
        </div>
    </div>
    <div class="body-nest">
        <section class="col-xs-12">
            <ul class="categories">
                {if (!empty($categories))}
                    {foreach from=$categories key=key item=cat}
                        <li>
                            <input type="checkbox" name="categories[]" {if $cat->id|in_array:$aData.categories}checked{/if} value="{$cat->id}" id="category_{$cat->id}">
                            <label for="category_{$cat->id}">{$cat->title}</label>
                            {if (!empty($cat->children))}
                                <ul>
                                    {foreach from=$cat->children key=key item=cat}
                                        <input type="checkbox" name="categories[]" {if $cat->id|in_array:$aData.categories}checked{/if} value="{$cat->id}" id="category_{$cat->id}">
                                        <label for="category_{$cat->id}">{$cat->title}</label>
                                    {/foreach}
                                </ul>
                            {/if}
                        </li>
                    {/foreach}
                {/if}
            </ul>
            <p class="form-text text-muted">{translate code='error_tags_limit' text='Максимальное количество тэгов 10'}</p>
        </section>
        <div class="clearfix"></div>
    </div>
</div>