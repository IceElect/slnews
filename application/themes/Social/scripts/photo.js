function photo(){
    var self = {};
    self.close_handler = false;
    self.upload_handler = false;

    self.albums = function(id, type, e){
        if(type){
            nav.load(setGet('albums', id), e, true);
        }else{
            nav.load(delGet('albums'), e, true);
            popup.remove('albums');
        }
    }
    self.album = function(id, type, e){
        if(type){
            nav.load(setGet('album', id), e, true);
        }else{
            nav.load(delGet('album'), e, true);
            popup.remove('album');
        }
    }
    self.photo = function(id, type, e){
        if(type){
            nav.load(setGet('photo', id), e, true);
            $(document).on('click', '.popup_layer', function(e){
                alert(1);
                if($(e.target).closest('.popup').length < 1)
                    self.photo(id, 0, e);
            })
        }else{
            $('.popup_layer').unbind('.popup_layer');
            nav.load(delGet('photo'), e, true);
            if($('body').width()<=480){
                $(".app-bar").animate({ bottom: '0px' }, 200);
            }
            popup.remove('photo');
        }
    }

    self.photoSet = function(id, type, e){
        var postData = {};
        $.post('/photo/'+id+'/set/'+type, postData, function(data){
            data = eval('('+data+')');
            if(data.response){
                switch(type){
                    case 'cover':
                        self.photo(id, false, e);
                    break;
                }
            }
        })
    }

    self.like = function(id, type, event){
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/photo/'+id+'/like/'+type,
            data: {},
            beforeSend: function () {
                
            },
            success: function (data){
                console.log(data);
                data = eval('('+data+')');
                if(data.response != false){
                    var likes = $(".photo[data-id="+id+"] span.likes_count"),
                        dislikes = $(".photo[data-id="+id+"] span.dislikes_count"),
                        likes_count = data.response['likes_count'],
                        dislikes_count = data.response['dislikes_count'];

                    if(likes_count == 0) likes_count = "";
                    if(dislikes_count == 0) dislikes_count = "";
                    likes.html(likes_count);
                    dislikes.html(dislikes_count);

                    if(type == 0){
                        likes.parent().addClass('selected');
                        dislikes.parent().removeClass('selected');
                    }else{
                        likes.parent().removeClass('selected');
                        dislikes.parent().addClass('selected');
                    }
                    if('remove' in data){
                        likes.parent().removeClass('selected');
                        dislikes.parent().removeClass('selected');
                    }
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };    

    self.answer = 0;
    self.setAnswer = function(comment_id, photo_id, e){
        self.answer = comment_id;
        self.showCommentForm(photo_id, e);

        return false;
    };

    self.comment = function(id){
        var text = $(".photo[data-id="+id+"] .send_form .field").html();
        var data = {text: text};
        if(self.answer)
            data.answer = self.answer;
        $.ajax({
            type: 'POST',
            url: '/photo/'+id+'/comment',
            data: data,
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    $(".photo[data-id="+id+"] .comments").append(data.response['html']);
                    $(".photo[data-id="+id+"] .send_form .field").html("");
                    $("abbr.timeago").timeago();
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };
    self.showCommentForm = function(id, e){
        e.preventDefault();
        $(".photo[data-id="+id+"] .send_holder").show();
        return false;
    }
    self.showCommentFormButtons = function(id, e){
        e.preventDefault();
        $(".photo[data-id="+id+"] .send_comment .buttons").show();
        return false;
    }
    self.hideCommentFormButtons = function(element, e){
        var id = $(element).attr('data-id');
        $('div').on('click', function(e){
            if(!$(".photo[data-id="+id+"]").is(':hover')){
                $(".photo[data-id="+id+"] .send_holder").hide();
            }
        })
        return false;
    }

    self.upload_popup = function(callback, album, e){
        e.preventDefault();

        $(document).unbind('submit');

        popup.show('photo_upload/avatar', {album: album});

        self.upload_handler = $('#avatar_upload form[data-type="upload"]').bind('submit', function(e){
            e.preventDefault();

            callback(123);
        });

    }
    self.avatar_upload = function(data){
        alert(data);
    }

    return self;
}
var photo = new photo();