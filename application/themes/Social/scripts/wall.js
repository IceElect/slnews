$(document).on('keypress', '#send-post-form #send-post-content', function(e){
    var form = $(this).closest('.send-post-form');
    var keyCode = e.keyCode || e.charCode || e.which;
    if (keyCode == 10 || keyCode == 13){
        if (!e.ctrlKey){
            /*
            var selection = window.getSelection(),
                range = selection.getRangeAt(0),
                br = document.createElement("br"),
                textNode = document.createTextNode("\u00a0");
            range.deleteContents();
            range.insertNode(br);
            range.collapse(false);
            range.insertNode(textNode);
            range.selectNodeContents(textNode);
            selection.removeAllRanges();
            selection.addRange(range);
            return false;*/
        }else{
            wall.addPost(form.attr('data-wall-id'),$(".btn[role=send]"));
            return false;
        }
        
        //return false;
    }
});
$(document).on('keypress', '.send-comment-form .send-form-area', function(e){
    var form = $(this).closest('.send-comment-form');
    var keyCode = e.keyCode || e.charCode || e.which;
    if (keyCode == 10 || keyCode == 13){
        if (!e.ctrlKey){
            wall.addComment(form.attr('data-post-id'));
            return false;
        }
        
        //return false;
    }
});
$(document).on('click', '#send-post-form .send-button', function(e){
    var form = $(this).closest('.send-post-form');
    wall.addPost(form.attr('data-wall-id'),$(".btn[role=send]"));
})

$(document).on('click', '.post .action-comment', function(e){
    e.preventDefault();
    var post = $(this).closest('.post-holder');
        post.find('.send-comment').toggleClass('show');
})

function onCtrlEnter(ev, handler) {
  ev = ev || window.event;
  if (ev.keyCode == 10 || ev.keyCode == 13 && (ev.ctrlKey || ev.metaKey && browser.mac)) {
      handler();
      cancelEvent(ev);
  }
}
$(function() {
    $(document).find('.wall-posts').infinitescroll({
        navSelector  : "div.nav",
        nextSelector : "div.nav a:first",
        itemSelector : ".wall-posts .post-holder",
        loading: {
            finishedMsg:'',
            img:'/logo.png'
        },
    });

    $("abbr.time").timeago();
})
function wall(){
    var post_attaches = [];

    this.post_attach = function(type, id){
        var types = {'file':'doc_select'};
        post_attaches.push(type+'_'+id);
        popup.hide(types.type);
    }

    this.addPost = function(wall_id, element){
        var content = $(".send-post-form #send-post-content").html();
        $.ajax({
            type: 'POST',
            url: '/ajax/post/add/'+wall_id,
            data: {content: content, attach: post_attaches},
            beforeSend: function () {
                $(element).html('Загрузка...');
            },
            success: function (data){
                data = eval('('+data+')');
                if('error' in data){
                    error(data.error);
                }else{
                    if(data.result && 'post' in data){
                        $(".wall-posts").prepend(data.post.data);
                        $(".send-post-form #send-post-content").html("");
                        $("abbr.time").timeago();
                        post_attaches = [];
                    }else{
                        error('Ошибка 11');
                    }
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        $(element).html('Опубликовать');
        return false;
    };

    this.deletePost = function(id, event){
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/post/'+id+'/delete',
            data: {},
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    $("[data-post="+id+"]").html(data.response).addClass('module');
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };
    this.returnPost = function(id, event){
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/post/'+id+'/return',
            data: {},
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    $("[data-post="+id+"]").replaceWith(data.response);
                    $("abbr.time").timeago();
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error(12);
            }
        });

        return false;
    };
    this.like = function(id, type, event){
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/ajax/post/like/'+id+'/'+type,
            data: {type: 'post'},
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    var likes = $("[data-post="+id+"] span.like-action"),
                        dislikes = $("[data-post="+id+"] span.dislike-action"),
                        rating = $("[data-post="+id+"] b.rating"),
                        likes_count = data.response['likes_count'],
                        rating_count = data.response['rating_count'],
                        dislikes_count = data.response['dislikes_count'];
                    rating.html(rating_count);
                    
                    if(type == 0){
                        likes.addClass('active');
                        dislikes.removeClass('active');
                    }
                    if(type == 1){
                        likes.removeClass('active');
                        dislikes.addClass('active');
                    }

                    if('remove' in data){
                        likes.removeClass('active');
                        dislikes.removeClass('active');
                    }
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };

    this.answer = 0;
    this.setAnswer = function(comment_id, post_id, e){
        this.answer = comment_id;
        this.showCommentForm(post_id, e);

        return false;
    };

    this.addComment = function(id){
        var text = $("[data-post="+id+"] .send-comment .send-form-area").html();
        var data = {text: text};
        if(this.answer)
            data.answer = this.answer;
        $.ajax({
            type: 'POST',
            url: '/ajax/post/comment/'+id,
            data: data,
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    $("[data-post="+id+"] .comments-list").append(data.post.data).removeClass('empty');

                    if($("[data-post="+id+"] .action-comment .action-count").html().length > 0)
                        $("[data-post="+id+"] .action-comment .action-count").html(parseInt($("[data-post="+id+"] .actions .comment .small").html())+1);
                    else
                        $("[data-post="+id+"] .action-comment .action-count").html(1);

                    $("[data-post="+id+"] .send-comment .send-form-area").html("");
                    $("abbr.time").timeago();
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };
    this.likeComment = function(id, type, event){
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/ajax/comment/like/'+id+'/'+type,
            data: {},
            beforeSend: function () {
                
            },
            success: function (data){
                data = eval('('+data+')');
                if(data.response != false){
                    var likes = $("[data-comment="+id+"] span.like-action"),
                        dislikes = $("[data-comment="+id+"] span.dislike-action"),
                        rating = $("[data-comment="+id+"] .comment-rating"),
                        likes_count = data.response['likes_count'],
                        rating_count = data.response['rating_count'],
                        dislikes_count = data.response['dislikes_count'];
                    rating.html(rating_count);
                    
                    if(type == 0){
                        likes.addClass('active');
                        dislikes.removeClass('active');
                    }
                    if(type == 1){
                        likes.removeClass('active');
                        dislikes.addClass('active');
                    }

                    if('remove' in data){
                        likes.removeClass('active');
                        dislikes.removeClass('active');
                    }
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });
        return false;
    };

    this.showSendPostForm = function(e){
        e.preventDefault();
        $(".send_post .block_footer").show();
        return false;
    };
    this.hideSendPostForm = function(e){
        $('div').on('click', function(e){
            if(!$(".send_post").is(':hover')){
                $(".send_post .block_footer").hide();
            }
        })
        return false;
    };

    this.showCommentForm = function(id, e){
        e.preventDefault();
        $("[data-post="+id+"] .send_holder").show();
        return false;
    }
    this.showCommentFormButtons = function(id, e){
        e.preventDefault();
        $("[data-post="+id+"] .send_comment .buttons").show();
        return false;
    }
    this.hideCommentFormButtons = function(element, e){
        var id = $(element).attr('data-id');
        $('div').on('click', function(e){
            if(!$("[data-post="+id+"]").is(':hover')){
                $("[data-post="+id+"] .send_holder").hide();
            }
        })
        return false;
    }
}
var wall = new wall();